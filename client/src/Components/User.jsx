import React, { Component } from 'react';

import Navbar from './Navbar';
import Admin from './Admin';
import Student from './Student'

class User extends Component {
    render() {
        return (
            <div>
                <Navbar/>
               {localStorage.isAdmin !== undefined && localStorage.isAdmin === "true" && (<Admin/>)}
               {localStorage.isAdmin !== undefined && localStorage.isAdmin === "false" && (<Student/>)}
            </div>
        );
    }
}

export default User;