import React, { Component } from "react";
import { Link } from "react-router-dom";
import { LockOutlined, UserOutlined } from "@ant-design/icons";
import { Button, Checkbox, Form, Input } from "antd";

import Navbar from "./Navbar";
import { login } from "./APILinks/login";

class UserLogin extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      isError: false,
      errorMessage: "",
    };
  }

  userEmail = (e) => {
    this.setState({
      email: e.target.value,
    });
  };

  userPassword = (e) => {
    this.setState({
      password: e.target.value,
    });
  };

  userLogin = async () => {
    let { email, password, isLogged } = this.state;

    let userCredentials = {
      email,
      password,
    };

    try {
      if (email !== "" && password !== "" && isLogged !== true) {
        let result = await login(userCredentials);
        if (result.hasOwnProperty("response")) {
          this.setState({
            isError: true,
            errorMessage: result.response.data.message,
          });
        } else {
          // console.log(result.data.user);
          localStorage.setItem("token",result.data.token);
          localStorage.setItem("isAdmin",result.data.user.isAdmin);
          localStorage.setItem("userId",result.data.user.id);
          this.props.history.push("/user");
        }
      }
    } catch (err) {
      this.setState({
        isError: true,
      });
    }
  };

  render() {
    return (
      <div>
        <Navbar />
        <div className="login-form-and-study-image" style={{ marginTop: 125 }}>
          <Form
            name="normal_login"
            className="login-form"
            onFinish={this.userLogin}
            style={{
              width: 750,
            }}
          >
            <img
              src="https://cdn-icons-png.flaticon.com/512/6681/6681204.png"
              alt="user login logo"
              className="login-page-lms-logo"
            ></img>
            <h1>Login</h1>
            <Form.Item
              name="username"
              rules={[
                { required: true, message: "Please input your Username!" },
              ]}
            >
              <Input
                prefix={<UserOutlined className="site-form-item-icon" />}
                placeholder="Enter Email"
                onChange={this.userEmail}
                type="email"
              />
            </Form.Item>
            <Form.Item
              name="password"
              rules={[
                { required: true, message: "Please input your Password!" },
              ]}
            >
              <Input
                prefix={<LockOutlined className="site-form-item-icon" />}
                type="password"
                placeholder="Enter Password"
                autoComplete="on"
                pattern="[a-zA-Z0-9]{3,16}"
                title="Password contain characters between a-z, A-Z and 0-9 and length in beetween 3-16"
                onChange={this.userPassword}
              />
            </Form.Item>
            <Form.Item>
              <Form.Item name="remember" valuePropName="checked" noStyle>
                <Checkbox>Remember me</Checkbox>
              </Form.Item>
            </Form.Item>

            <Form.Item className="login-button-and-register-content">
              <Button
                type="primary"
                htmlType="submit"
                className="login-form-button"
              >
                Log in
              </Button>
            </Form.Item>

            {this.state.isError === true && (
              <div className="login-invalid-credential-message">
                <p> {this.state.errorMessage} </p>
              </div>
            )}
            <pre className="login-form-forgot">
              <Link to={"/signup"}>Register Now!</Link>
            </pre>
          </Form>
          <img
            src="https://uploads-ssl.webflow.com/5ef061301549585c93061932/6120c963cfcef06610415e40_Personalized%20and%20paced%20learning.jpg"
            alt="studing"
            className="login-page-study-image"
          ></img>
        </div>
      </div>
    );
  }
}

export default UserLogin;
