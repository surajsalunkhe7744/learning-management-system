import React, { Component } from "react";
import { Modal, Button, Form, Input, message } from "antd";
import { LockOutlined, UserOutlined, GoogleOutlined } from "@ant-design/icons";
import { Link } from "react-router-dom";

import { removeUser } from "./APILinks/removeUser";
import { user } from "./APILinks/getOneUser";
import { updateUser } from "./APILinks/updateUser";

class Student extends Component {
  constructor(props) {
    super(props);

    this.state = {
      user: {},
      firstName: "",
      lastName: "",
      email: "",
      password: "",
      //   rePassword: "",
      isError: false,
      errorMessage: "",
      visible: false,
    };
  }

  componentDidMount = async () => {
    try {
      let result = await user();
      this.setState({
        user: result.user,
        firstName: result.user.firstName,
        lastName: result.user.lastName,
        email: result.user.email,
      });
    } catch (err) {
      this.setState({
        isError: true,
      });
    }
  };

  // Handle User Profile Pop-up

  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  handleUpdateUser = async () => {
    this.setState({
      visible: false,
    });

    let user = {
      firstName: this.state.firstName,
      lastName: this.state.lastName,
      email: this.state.email,
    };

    if (
      this.state.password !== null &&
      this.state.password !== "" &&
      this.state.password !== undefined
    ) {
      user["password"] = this.state.password;
    }

    try {
      let result = await updateUser(user);
      if (result.hasOwnProperty("response")) {
        this.setState({
          isError: true,
          errorMessage: result.response.data.error,
          firstName: this.state.user.firstName,
          lastName: this.state.user.lastName,
          email: this.state.user.email,
        });
      } else {
        message.config({ top: 125, duration: 2 });
        message.success("Your profile updated successfully.");
      }
    } catch (err) {
      this.setState({
        isError: true,
      });
    }
  };

  handleCancel = () => {
    this.setState({
      visible: false,
    });
  };

  // Handle user Profile Update

  userFirstName = (e) => {
    this.setState({
      firstName: e.target.value,
    });
  };

  userLastName = (e) => {
    this.setState({
      lastName: e.target.value,
    });
  };

  userEmail = (e) => {
    this.setState({
      email: e.target.value,
    });
  };

  userPassword = (e) => {
    this.setState({
      password: e.target.value,
    });
  };

  handleDeleteRequest = async () => {
    try {
      let result = await removeUser();
      if (result.statusText === "OK") {
        localStorage.clear();
        message.config({ top: 125, duration: 2 });
        message.success("Your profile updated successfully.");
      } else {
        this.setState({
          isError: true,
        });
      }
    } catch (err) {
      this.setState({
        isError: true,
      });
    }
  };

  render() {
    return (
      <div>
        {this.state.isError === false && (
          <div className="user-and-admin">
            <img
              src="https://t3.ftcdn.net/jpg/02/59/36/44/360_F_259364433_78jm5AbTPxVDaxk3CrhKF4ZwIoWBSzzQ.webp"
              alt="student-operations"
              style={{ height: 350, width: 450, marginTop: 175 }}
            />
            <div className="user-all-operation">
              <p className="user-title">Student</p>
              <div className="user-operation-filed">
                Update Profile
                <Button type="primary" onClick={this.showModal}>
                  Enter
                </Button>
                <Modal
                  open={this.state.visible}
                  onOk={this.handleUpdateUser}
                  onCancel={this.handleCancel}
                >
                  <Form
                    name="normal_signup"
                    initialValues={{
                      firstName: this.state.user.firstName,
                      lastName: this.state.user.lastName,
                      email: this.state.user.email,
                    }}
                  >
                    <h1>Student</h1>
                    <p className="profile-form-label">
                      FirstName<span style={{ color: "red" }}>*</span>
                    </p>
                    <Form.Item
                      name="firstName"
                      rules={[
                        {
                          required: true,
                          message: "Please enter your first name!",
                        },
                      ]}
                    >
                      <Input
                        prefix={
                          <UserOutlined className="site-form-item-icon" />
                        }
                        placeholder="Enter First Name"
                        onChange={this.userFirstName}
                        value={this.state.user.firstName}
                        pattern="[a-zA-Z]{1,40}"
                        title="firstName must contain characters in between a-z, A_Z and should have length in between 1 to 40"
                      />
                    </Form.Item>
                    <p className="profile-form-label">
                      LastName<span style={{ color: "red" }}>*</span>
                    </p>
                    <Form.Item
                      name="lastName"
                      rules={[
                        {
                          required: true,
                          message: "Please enter your last name!",
                        },
                      ]}
                    >
                      <Input
                        prefix={
                          <UserOutlined className="site-form-item-icon" />
                        }
                        placeholder="Enter Last Name"
                        onChange={this.userLastName}
                        value={this.state.user.lastName}
                        pattern="[a-zA-Z]{1,40}"
                        title="firstName must contain characters in between a-z, A_Z and should have length in between 1 to 40"
                      />
                    </Form.Item>
                    <p className="profile-form-label">
                      Email<span style={{ color: "red" }}>*</span>
                    </p>
                    <Form.Item
                      name="email"
                      rules={[
                        { required: true, message: "Please enter your email!" },
                      ]}
                    >
                      <Input
                        prefix={
                          <GoogleOutlined className="site-form-item-icon" />
                        }
                        placeholder="Enter Email"
                        type="email"
                        value={this.state.user.email}
                        onChange={this.userEmail}
                      />
                    </Form.Item>
                    <p className="profile-form-label">New Password</p>
                    <Form.Item
                      name="password"
                      rules={[
                        {
                          required: true,
                          message: "Please enter your Password!",
                        },
                      ]}
                    >
                      <Input
                        prefix={
                          <LockOutlined className="site-form-item-icon" />
                        }
                        type="password"
                        autoComplete="on"
                        placeholder="Enter Password"
                        pattern="[a-zA-Z0-9]{3,16}"
                        title="Password contain characters between a-z, A-Z and 0-9 and length in beetween 3-16"
                        onChange={this.userPassword}
                      />
                    </Form.Item>
                    {/* {this.state.errorMessage !== "" && (
                      <div className="login-invalid-credential-message">
                        <p>{this.state.errorMessage}</p>
                      </div>
                    )} */}
                  </Form>
                </Modal>
              </div>
              <div className="user-operation-filed">
                View All Courses
                <Link to={"all/courses"}>
                  <Button type="primary">Enter</Button>
                </Link>
              </div>
              <div className="user-operation-filed">
                View Enrolled Course List
                <Button type="primary">Enter</Button>
              </div>
              <div className="user-operation-filed">
                Delete My Account
                <Link to={"/login"}>
                  <Button
                    type="primary"
                    danger
                    onClick={this.handleDeleteRequest}
                  >
                    Enter
                  </Button>
                </Link>
              </div>
            </div>
          </div>
        )}
        {this.state.isError && (
          <div className="error-field">
            <img
              src="https://media.tenor.com/NRoUnno3Wr4AAAAM/iranserver-iran.gif"
              alt="error-image"
              className="error-image"
            ></img>
            <h1 style={{ color: "red" }}>{this.state.errorMessage}</h1>
            {setTimeout(() => {
              this.setState({
                isError: false,
              });
            }, 2000)}
          </div>
        )}
      </div>
    );
  }
}

export default Student;
