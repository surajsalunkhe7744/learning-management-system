import React from "react";
import { Carousel } from "antd";

import CourseCard from "./CourseCard";

const Courses = ({ courses }) => {
  return (
    <div className="courses">
      <Carousel style={{ width: 440 }} autoplay>
        {courses.map((course) => <CourseCard {...course} key={course.id}/>)}
      </Carousel>
    </div>
  );
}

export default Courses;
