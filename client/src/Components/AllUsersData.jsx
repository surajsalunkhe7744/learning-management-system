import React, { Component } from "react";

import Navbar from "./Navbar";
import { users } from "./APILinks/getUsers";
import DisplayAllUsers from "./DisplayAllUsers";

class AllUsersData extends Component {
  constructor(props) {
    super(props);

    this.state = {
      users: [],
      isLoaded: false,
      isError: false,
    };
  }

  componentDidMount = async () => {
    try {
      let response = await users();
  
      response
        ? this.setState({
            users: response,
            isLoaded: true,
          })
        : this.setState({
            isError: true,
          });
    } catch (err) {
      console.log(err.message);
      this.setState({
        isError: true,
      });
    }
  };

  render() {
    let { users, isLoaded, isError } = this.state;
    return (
      <div>
        <Navbar />
        {isLoaded === false && isError === false && (
          <h1 className="loading-message">Loading ...</h1>
        )}
        {isLoaded && isError === false && users.length === 0 && (
          <h1 className="loading-message">Users not found ...</h1>
        )}
        {isError && (
          <h1 className="loading-message">
            Error while fetching the users data ...
          </h1>
        )}
        {isLoaded && isError === false && users.length !== 0 && (
          <div className="learning-importance-block">
            <DisplayAllUsers users={users} />
          </div>
        )}
      </div>
    );
  }
}

export default AllUsersData;
