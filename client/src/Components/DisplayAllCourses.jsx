import React, { Component } from "react";
import { Button, Card, Space, message } from "antd";

import { courses } from "./APILinks/getCourses";
import { enrolledToCourse } from "./APILinks/enrolleToCourse";
import { removeCourse } from "./APILinks/removeCourse";
import Navbar from "./Navbar";

const { Meta } = Card;

class DisplayAllCourses extends Component {
  constructor(props) {
    super(props);

    this.state = {
      courses: [],
      enrolledCourses: [],
      isLoaded: false,
      isError: false,
    };
  }

  componentDidMount = async () => {
    try {
      let response = await courses();

      response
        ? this.setState({
            courses: response,
            isLoaded: true,
          })
        : this.setState({
            isError: true,
          });
    } catch (err) {
      console.log(err.message);
      this.setState({
        isError: true,
      });
    }
  };

  handleCourseEnrollement = async (id) => {
    try {
      let enrollementDetails = {
        courseId: id,
        userId: Number(localStorage.userId),
      };

      let result = await enrolledToCourse(enrollementDetails);
      // console.log(result);
      if (!result.hasOwnProperty("response")) {
        message.config({ top: 125, duration: 2 });
        message.success("Enrolled Successfully.");
      } else {
        message.config({ top: 125, duration: 2 });
        message.warning("Already Enrolled.");
      }
    } catch (err) {
      this.setState({
        isError: true,
      });
    }
  };

  handleDeleteCourse = async (id) => {
    try {
      let result = await removeCourse(id);
      if (!result.hasOwnProperty("response")) {
        message.config({ top: 125, duration: 2 });
        message.success("Course Deleted Successfully.");
        this.setState({
          courses: [...this.state.courses.filter(course => course.id !== id)]
        });
      } else {
        this.setState({
          isError: true,
        });
      }
    }
    catch (err) {
      this.setState({
        isError: true,
      });
    }
  }

  render() {
    let { isLoaded, isError, courses, enrolledCourses } = this.state;
    return (
      <div>
        <Navbar />
        {isLoaded === false && isError === false && (
          <h1 className="loading-message">Loading ...</h1>
        )}
        {isLoaded && isError === false && courses.length === 0 && (
          <h1 className="loading-message">Cources not found ...</h1>
        )}
        <div className="display-all-courses">
          {isLoaded &&
            isError === false &&
            courses.length > 0 &&
            courses.map((course) => {
              let { id, image, title } = course;
              return (
                <Card
                  hoverable
                  style={{ width: 350, marginBottom: "6%" }}
                  cover={<img alt="example" src={image} />}
                  key={id}
                >
                  {localStorage.isAdmin === "true" ? (
                    <div className="display-all-courses-footer">
                      <Meta title={title} />
                      <Space>
                        <Button type="primary" ghost>
                          Edit
                        </Button>
                        <Button danger ghost onClick={ () => this.handleDeleteCourse(id) }>
                          Remove
                        </Button>
                      </Space>
                    </div>
                  ) : (
                    <div className="display-all-courses-footer">
                      {enrolledCourses.length === 0 ? (
                        <>
                          <Meta title={title} />
                          <Space>
                            <Button
                              type="primary"
                              ghost
                              onClick={() => this.handleCourseEnrollement(id)}
                            >
                              Enroll
                            </Button>
                          </Space>
                        </>
                      ) : (
                        <>
                          <Meta title={title} />
                          <Space>
                            <Button
                              type="primary"
                              onClick={() => this.handleCourseChapter(id)}
                            >
                              Study
                            </Button>
                            <Button
                              type="primary"
                              ghost
                              onClick={() => this.handleCourseChapter(id)}
                            >
                              Delete
                            </Button>
                          </Space>
                        </>
                      )}
                    </div>
                  )}
                </Card>
              );
            })}
        </div>
        {this.state.isError && (
          <div className="error-field">
            <img
              src="https://media.tenor.com/NRoUnno3Wr4AAAAM/iranserver-iran.gif"
              alt="error-image"
              className="error-image"
            ></img>
            <h1 style={{ color: "red" }}>Error while deleting the course</h1>
            {setTimeout(() => {
              this.setState({
                isError: false,
              });
            }, 2000)}
          </div>
        )}
      </div>
    );
  }
}

export default DisplayAllCourses;
