import React from 'react';
import { Card } from 'antd';

const { Meta } = Card;

let CourseCard = ({ id, image, title }) => {
    return (
        <Card
    hoverable
    style={{ width: 440 }}
    cover={<img alt="example" src={image} />}
    key={id}
    >
     <Meta title={title} />
    </Card>
    );
}

export default CourseCard;