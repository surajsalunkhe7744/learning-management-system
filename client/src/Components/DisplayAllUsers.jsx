import React, { Component } from 'react';
import { Table, Space, Button, message } from 'antd';

import { makeAdmin } from './APILinks/makeAdmin';
import { removeAdmin } from './APILinks/removeAdmin';

class DisplayAllUsers extends Component {

  constructor(props) {
    super(props);

    this.state = {
      users: this.props.users,
    }
  }

  handleAdminMaker = async (user) => {
    try {
      console.log(user.id);
      let result = await makeAdmin(user.id);

      if(!result.hasOwnProperty("response")) {

        this.setState({
          users: this.state.users.map(userData => {
            if(userData.id === user.id) {
              userData.isAdmin = true;
            }
            return userData;
          })
        });
        
      }
    }
    catch (err) {
      console.log(err);
    }
  }

  handleAdminRemover = async (user) => {
    try {
      console.log(user.id);
      let result = await removeAdmin(user.id);

      if(!result.hasOwnProperty("response")) {

        this.setState({
          users: this.state.users.map(userData => {
            if(userData.id === user.id) {
              userData.isAdmin = false;
            }
            return userData;
          })
        });
        
      } else {
        message.config({ top: 125, duration: 2 });
        message.warning("This. is your account, can't change!.");
      }
    }
    catch (err) {
      console.log(err);
    }
  }

    render() {

        const columns = [
            {
              title: 'Sr. No.',
              dataIndex: 'id',
              key: 'id',
            },
            {
              title: 'First Name',
              dataIndex: 'firstName',
              key: 'firstName',
            },
            {
                title: 'Last Name',
                dataIndex: 'firstName',
                key: 'firstName',
            },
            {
                title: 'Email',
                dataIndex: 'email',
                key: 'email',
            },
            {
              title: 'Created At',
              dataIndex: 'createdAt',
              key: 'Created At',
            },
            {
              title: 'isAdmin',
              dataIndex: 'isAdmin',
              key: 'isAdmin',
              render: (isAdmin,id) => isAdmin ? (
                <Space>
                  <span>Admin</span>
                         <Button type="primary" danger onClick={ () => this.handleAdminRemover(id) }>
                           Remove
                         </Button>
                       </Space>
              ) : (
                  <Button type="primary" ghost onClick={ () => this.handleAdminMaker(id) }>
                           Make Admin
                         </Button>
              ),
            },
          ];

        return (
            <Table dataSource={this.props.users} rowKey={user => user.id} columns={columns} className='display-user-table' />
        );
    }
}

export default DisplayAllUsers;