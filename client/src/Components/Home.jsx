import React, { Component } from "react";

import { courses } from "./APILinks/getCourses";
import Navbar from "./Navbar";
import Courses from "./Courses";

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      courses: [],
      isLoaded: false,
      isError: false,
    };
  }

  componentDidMount = async () => {
    try {
      let response = await courses();

      response
        ? this.setState({
            courses: response,
            isLoaded: true,
          })
        : this.setState({
            isError: true,
          });
    } catch (err) {
      console.log(err.message);
      this.setState({
        isError: true,
      });
    }
  };
  render() {
    let { courses, isLoaded, isError } = this.state;
    return (
      <div>
        <Navbar />
        {isLoaded === false && isError === false && (
          <h1 className="loading-message">Loading ...</h1>
        )}
        {isLoaded && isError === false && courses.length === 0 && (
          <h1 className="loading-message">Cources not found ...</h1>
        )}
        {isError && (
          <h1 className="loading-message">
            Error while fetching the cources data ...
          </h1>
        )}
        {isLoaded && isError === false && courses.length !== 0 && (
          <div className="learning-importance-block">
            <p className="learning-importance-heading">
              ** Be educated so that you can change the world. **
            </p>
            <img
              src="https://thumbs.gfycat.com/PlumpOptimalFlyingfox-max-1mb.gif"
              alt="book"
              className="learning-importance-block-image"
            ></img>
            <p className="learning-importance-para">
              Continuous learning is important because it helps people to feel
              happier and more fulfilled in their lives and careers, and to
              maintain stronger cognitive functioning when they get older.
              Making learning continuous helps companies boost their
              productivity, profitability, adaptability to change, and potential
              to innovate in their industries. Learning is important to society
              as a whole because it helps different groups of people to share
              knowledge, agree on mutual values, and understand one another
              better.
            </p>
            <p className="our-courses-heading">** Our Courses **</p>
            <Courses courses={courses} />
          </div>
        )}
      </div>
    );
  }
}

export default Home;
