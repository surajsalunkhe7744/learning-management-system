import React, { Component } from "react";
import { Link } from "react-router-dom";

class Navbar extends Component {
  render() {
    return (
      <div className="header">
        <div className="logo-and-name">
          <img
            src="https://eoscfuture.eu/wp-content/uploads/2022/09/EOSC-KER6-Knowledge-317x317.png"
            alt="logo"
            className="logo"
          ></img>
          <p className="learning-management-system-name">
            Learning Management System
          </p>
        </div>
        <ul className="navbar">
          <Link to={"/"} style={{ textDecoration: "none" }}>
            <li className="navbar-element">Home</li>
          </Link>
          {localStorage.length === 0 && (
            <Link to={"/login"} style={{ textDecoration: "none" }}>
              <li className="navbar-element">Login</li>
            </Link>
          )}
          {localStorage.length !== 0 && (
            <Link to={"/user"} style={{ textDecoration: "none" }}>
              <li className="navbar-element">
              {localStorage.isAdmin === "true" ? "Admin Menu":"Student Menu"}
              </li>
            </Link>
          )}
          {localStorage.length !== 0 && (
            <Link to={"/login"} style={{ textDecoration: "none" }}>
              <li className="navbar-element" onClick={() => localStorage.clear()}>Logout</li>
            </Link>
          )}
        </ul>
      </div>
    );
  }
}

export default Navbar;
