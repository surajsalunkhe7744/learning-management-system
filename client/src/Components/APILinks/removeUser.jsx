import axios from "axios";

const BASE_URL = process.env.REACT_APP_BASE_URL;
const PORT = process.env.REACT_APP_PORT;

export let removeUser = async () => {
  try {
    let response = await axios.delete(`${BASE_URL}:${PORT}/api/user/remove`, {
      headers: {
        Authorization: `Bearer ${localStorage.token}`,
      },
    });
    return response;
  } catch (err) {
    console.log(err);
    return err;
  }
};
