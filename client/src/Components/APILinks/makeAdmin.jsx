import axios from 'axios';

const BASE_URL = process.env.REACT_APP_BASE_URL;
const PORT = process.env.REACT_APP_PORT;


export let makeAdmin = async (id) => {
    try {
        let response = await axios.patch(`${BASE_URL}:${PORT}/api/user/admin/${id}`,{isAdmin:true},{
            headers: {
              Authorization: `Bearer ${localStorage.token}`,
            },
          });
        return response.data;
    } catch (err) {
    return err;
  }
};