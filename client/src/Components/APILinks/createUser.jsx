import axios from 'axios';

const BASE_URL = process.env.REACT_APP_BASE_URL;
const PORT = process.env.REACT_APP_PORT;


export let createUser = async (user) => {
    try {
        let response = await axios.post(`${BASE_URL}:${PORT}/api/user/create`,user);
        return response.data;
    } catch (err) {
    return err;
  }
};