import axios from "axios";

const BASE_URL = process.env.REACT_APP_BASE_URL;
const PORT = process.env.REACT_APP_PORT;

export let removeCourse = async (id) => {
  try {
    let response = await axios.delete(`${BASE_URL}:${PORT}/api/course/remove/${id}`, {
      headers: {
        Authorization: `Bearer ${localStorage.token}`,
      },
    });
    return response;
  } catch (err) {
    console.log(err);
    return err;
  }
};