import axios from "axios";

const BASE_URL = process.env.REACT_APP_BASE_URL;
const PORT = process.env.REACT_APP_PORT;


export let courses = async () => {
  try {
    let response = await axios.get(`${BASE_URL}:${PORT}/api/courses`);
    return response.data;
  } catch (err) {
    return false;
  }
};
