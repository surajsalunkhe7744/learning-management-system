import axios from "axios";

const BASE_URL = process.env.REACT_APP_BASE_URL;
const PORT = process.env.REACT_APP_PORT;


export let login = async (loginCredentials) => {
  try {
    let response = await axios.post(`${BASE_URL}:${PORT}/api/user/login`,loginCredentials);
    return response;
  } catch (err) {
    return err;
  }
};