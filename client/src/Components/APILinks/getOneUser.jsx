import axios from "axios";

const BASE_URL = process.env.REACT_APP_BASE_URL;
const PORT = process.env.REACT_APP_PORT;


export let user = async () => {
  try {
    let response = await axios.get(`${BASE_URL}:${PORT}/api/user/profile`,{
        headers: {
          Authorization: `Bearer ${localStorage.token}`,
        },
      });
    return response.data;
  } catch (err) {
    return false;
  }
};