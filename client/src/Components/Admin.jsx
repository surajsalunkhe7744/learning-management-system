import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Button } from 'antd';

class Admin extends Component {
    render() {
        return (
                <div className="user-and-admin">
                    <img src="https://t3.ftcdn.net/jpg/02/60/44/90/240_F_260449024_Wh7y8ZcRH9WPVlExw6qVHho6HaGcn57A.jpg" alt="admin-operation-page-image" style={{height:350, width: 450, marginTop:175}}></img>
                    <div className="user-all-operation">
                        <p className="user-title">Admin</p>
                        <div className="user-operation-filed">
                            Display All Courses
                            <Link to={"all/courses"}>
                                <Button type="primary">Enter</Button>
                            </Link>
                        </div>
                        <div className="user-operation-filed">
                            Display all Users
                            <Link to={"/all/users"}>
                                <Button type="primary">Enter</Button>
                            </Link>
                        </div>
                        <div className="user-operation-filed">
                            Add Course
                            <Button type="primary">Enter</Button>
                        </div>
                    </div>
                </div>
        );
    }
}

export default Admin;