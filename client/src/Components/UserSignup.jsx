import { Component } from "react";
import { LockOutlined, UserOutlined, GoogleOutlined } from "@ant-design/icons";
import { Button, Checkbox, Form, Input } from "antd";

import Navbar from "./Navbar";
import { createUser } from "./APILinks/createUser";

class UserSignup extends Component {
  constructor(props) {
    super(props);

    this.state = {
      firstName: "",
      lastName: "",
      email: "",
      password: "",
      repassword: "",
      errorMessage: "",
      isError: false,
    };
  }

  userFirstName = (e) => {
    this.setState({
      firstName: e.target.value,
    });
  };

  userLastName = (e) => {
    this.setState({
      lastName: e.target.value,
    });
  };

  userEmail = (e) => {
    this.setState({
      email: e.target.value,
    });
  };

  userPassword = (e) => {
    this.setState({
      password: e.target.value,
    });
  };

  userRePassword = (e) => {
    this.setState({
      repassword: e.target.value,
    });
  };

  createUser = async () => {
    let { firstName, lastName, email, password, repassword } = this.state;

    let user = {
      firstName,
      lastName,
      email,
      password,
      repassword,
    };

    try {
      if (
        firstName !== "" &&
        lastName !== "" &&
        email !== "" &&
        password !== "" &&
        repassword !== ""
      ) {
        let result = await createUser(user);
        if (!result.hasOwnProperty("addedUser")) {
          this.setState({
            isError: true,
            errorMessage: result.response.data.message,
          });
        } else {
          this.props.history.push("/login");
        }
      }
    } catch (err) {
      this.setState({
        isError: true,
      });
    }
  };

  render() {
    return (
      <div>
        <Navbar />
        <div className="signup">
          <Form
            name="normal_signup"
            className="signup-form"
            initialValues={{ remember: true }}
            onFinish={(e) => {
              this.createUser(e);
            }}
          >
            <img
              src="https://cdn-icons-png.flaticon.com/512/6681/6681204.png"
              alt="logo"
              className="login-page-lms-logo"
            ></img>
            <h1>User Sign Up</h1>
            <Form.Item
              name="firstName"
              rules={[
                { required: true, message: "Please enter your first name!" },
              ]}
            >
              <Input
                prefix={<UserOutlined className="site-form-item-icon" />}
                placeholder="Enter First Name"
                onChange={this.userFirstName}
                pattern="[a-zA-Z]{1,40}"
                title="firstName must contain characters in between a-z, A_Z and should have length in between 1 to 40"
              />
            </Form.Item>
            <Form.Item
              name="lastName"
              rules={[
                { required: true, message: "Please enter your last name!" },
              ]}
            >
              <Input
                prefix={<UserOutlined className="site-form-item-icon" />}
                placeholder="Enter Last Name"
                onChange={this.userLastName}
                pattern="[a-zA-Z]{1,40}"
                title="firstName must contain characters in between a-z, A_Z and should have length in between 1 to 40"
              />
            </Form.Item>
            <Form.Item
              name="email"
              rules={[{ required: true, message: "Please enter your email!" }]}
            >
              <Input
                prefix={<GoogleOutlined className="site-form-item-icon" />}
                placeholder="Enter Email"
                type="email"
                onChange={this.userEmail}
              />
            </Form.Item>
            <Form.Item
              name="password"
              rules={[
                { required: true, message: "Please enter your Password!" },
              ]}
            >
              <Input
                prefix={<LockOutlined className="site-form-item-icon" />}
                type="password"
                autoComplete="on"
                placeholder="Enter Password"
                pattern="[a-zA-Z0-9]{3,16}"
                title="Password contain characters between a-z, A-Z and 0-9 and length in beetween 3-16"
                onChange={this.userPassword}
              />
            </Form.Item>
            <Form.Item
              name="re-password"
              rules={[
                { required: true, message: "Please enter your Password!" },
              ]}
              onChange={this.userRePassword}
            >
              <Input
                prefix={<LockOutlined className="site-form-item-icon" />}
                type="password"
                autoComplete="on"
                pattern="[a-zA-Z0-9]{3,16}"
                title="Password contain characters between a-z, A-Z and 0-9 and length in beetween 3-16"
                placeholder="Re-Enter Password"
              />
            </Form.Item>
            <Form.Item>
              <Form.Item name="remember" valuePropName="checked" noStyle>
                <Checkbox>Remember me</Checkbox>
              </Form.Item>
            </Form.Item>

            <Form.Item className="login-button-and-register-content">
              <Button
                type="primary"
                htmlType="submit"
                className="login-form-button"
              >
                Sign Up
              </Button>
            </Form.Item>
            {this.state.isError && (
              <div className="login-invalid-credential-message">
                <p>{this.state.errorMessage}</p>
              </div>
            )}
          </Form>
        </div>
      </div>
    );
  }
}

export default UserSignup;
