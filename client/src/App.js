import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import "./App.css";
import Home from "./Components/Home";
import UserLogin from "./Components/UserLogin";
import UserSignup from "./Components/UserSignup";
import User from "./Components/User"
import AllUsersData from "./Components/AllUsersData";
import DisplayAllCourses from "./Components/DisplayAllCourses";

function App() {
  return (
    <div>
      <Router>
      <div className="container-body">
          <Switch>
              <Route exact path="/" component={Home} />
              <Route path="/login" component={UserLogin} />
              <Route path="/signup" component={UserSignup} />
              <Route path="/user" component={User} />
              <Route path="/all/users" component={AllUsersData} />
              <Route path="/all/courses" component={DisplayAllCourses} />
          </Switch>
        </div>
      </Router>
    </div>
  );
}

export default App;
