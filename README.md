## Learning management System

### Components Versions
* React - 18.2.0 Version
* Node JS - 18.14.0 Version
* Chrome - 109.0.5414.119 Version

### Folder Structure
* client - Contain react app
* server - Contain Express and Sequalize folders and files

### Used NPM Libraries
* @hapi/joi
* bcrypt
* body-parser
* cors
* express
* jsonwebtoken
* mysql2
* nodemon
* sequelize
* react
* react-router-dom
* antD

### Testing API's using Postman

1. Course
* Get all courses - http://localhost:8080/api/courses
* Add one course - http://localhost:8080/course/create
* Update one course - http://localhost:8080/course/update/:id
* Remove one course - http://localhost:8080/course/remove/:id

2. User
* Create user - http://localhost:8080/user/create
* Update user - http://localhost:8080/user/update
* Delete user - http://localhost:8080/user/remove
* User login - http://localhost:8080/user/login

### To run this project
* Server is running on port 8080
* For starting the server use command - *nodemon server.js*
* For Starting the client (react app) run command - *npm start* 
