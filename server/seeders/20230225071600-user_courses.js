'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
     await queryInterface.bulkInsert('user_courses', [
      {
     UserId: 1,
     CourseId: 1,
     createdAt: new Date(),
      updatedAt: new Date(),
     }
    ], {});
  },

  async down (queryInterface, Sequelize) {
     await queryInterface.bulkDelete('user_courses', null, {});
  }
};
