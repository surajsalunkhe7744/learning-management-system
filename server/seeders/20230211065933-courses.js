"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      "courses",
      [
        {
          image:
            "https://miro.medium.com/max/1400/1*xdo0UBpyszvD7-7EH4TkIA.png",
          title: "Node JS",
          content: 'Will update later',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          image:
            "https://pixelmechanics.com.sg/wp-content/uploads/2019/06/html5-logo-for-web-development.png",
          title: "HTML",
          content: 'Will update later',
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          image: "https://www.codespot.org/assets/css.jpg",
          title: "CSS",
          content: 'Will update later',    
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          image: "https://miro.medium.com/max/1400/0*kfom-b3fk4IqgDgc.png",
          title: "React JS",
          content: 'Will update later',        
          createdAt: new Date(),
          updatedAt: new Date()
        },
        {
          image: "https://www.filepicker.io/api/file/sJCfo5xMRL6Blf9H6OSP",
          title: "Redux",
          content: 'Will update later',
          createdAt: new Date(),
          updatedAt: new Date()
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    {
      return queryInterface.bulkDelete('courses', null, {});
    }
  },
};
