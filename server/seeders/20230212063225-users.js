"use strict";

const bcrypt = require("bcrypt");

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.bulkInsert(
      "users",
      [
        {
          firstName: "Virat",
          lastName: "Kohli",
          email: "virat99@gmail.com",
          password: bcrypt.hashSync("Virat99", 10),
          isAdmin: false,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          firstName: "Sachin",
          lastName: "Tendulkar",
          email: "sachin99@gmail.com",
          password: bcrypt.hashSync("Sachin99", 10),
          isAdmin: true,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          firstName: "Rohit",
          lastName: "Sharma",
          email: "rohit99@gmail.com",
          password: bcrypt.hashSync("Rohit99", 10),
          isAdmin: false,
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete("users", null, {});
  },
};
