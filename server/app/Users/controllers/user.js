const { addUser } = require("../services/addUser");
const { updateUserData } = require("../services/updateUser");
const { removeUser } = require("../services/removeUser");
const { userLoginHandler } = require("../services/userLogin");
const { getUsers } = require("../services/getUsers");
const { getOneUser } = require("../services/getOneUser");
const { userCourseEnrollment } = require("../services/enrollToCourse");
const { makeAdmin } = require("../services/makeAdmin");
const { removeAdmin } = require("../services/removeAdmin");


const getAllUser =  (req, res) => {
    getUsers(req,res);
}

const createUser =  (req, res) => {
     addUser(req,res);
}

const updateUser =  (req, res) => {
     updateUserData(req,res);
}

const userLogin = (req, res) => {
    userLoginHandler(req,res);
}

const getSingleUser = (req, res) => {
    getOneUser(req,res);
}

const deleteUser = (req,res) => {
    removeUser(req,res);
}

const enrollToCourse = (req,res) => {
    userCourseEnrollment(req,res);
}

const studentToAdmin = (req,res) => {
    makeAdmin(req,res);
}

const adminToStudent = (req,res) => {
    removeAdmin(req,res);
}

module.exports = {
    createUser,
    userLogin,
    updateUser,
    deleteUser,
    getAllUser,
    getSingleUser,
    enrollToCourse,
    studentToAdmin,
    adminToStudent
}