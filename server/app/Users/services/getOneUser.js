const { Op } = require("sequelize");

const { user } = require("../../../models");

async function getOneUser(req, res) {
    try {
        let getUser = await user.findOne({
            where: {
                    id: req.userData.id,
                }
        });

        if(getUser.dataValues.id === req.userData.id && getUser !== undefined) {
            res.status(200).json({
                Message: `The user was found successfully.`,
                user: getUser.dataValues,
            })
        } else {
            res.status(404).json({
                Message: `The user was not found.`
            })
        } 
    } catch(error) {
        return res.status(500).json({
            message: error
        });
    }
}

module.exports = {
    getOneUser
}