const bcrypt = require("bcrypt");

const { user } = require("../../../models");
// const { schema } = require("../Validation/userValidation");

async function removeAdmin(req, res) {
  try {
    // console.log(req.body); 
    // await schema.validateAsync(req.body);

    if(!req.userData.isAdmin) {
        return res.status(400).json({
            Message: `The admin user should only allow to remove another admin.`
        })
    }

    console.log(req.params.id);

    if(req.userData.id === Number(req.params.id)) {
        return res.status(400).json({
            Message: `The admin can't remove themself from admin user.`
        })
    }

    let getUserDetails = await user.findOne({
        where: {
                id: req.params.id,
            }
    });

    // console.log(getUserDetails);

    if(!getUserDetails.isAdmin) {
        return res.status(400).json({ message: `The user is already not a admin user.`});
    }

    let updatedUser = await user.update(req.body, {
      where: {
        id: req.params.id,
      },
    });

    // console.log(updatedUser);

    if(updatedUser !== undefined && updatedUser[0] === 1) {
      res.status(200).json({ message: `User profile is updated to Admin.` });
    } else {
      res.status(404).json({ message: `User was not found.` });
    }
  } catch (error) {
    console.log(error);
    return res.status(500).json({ error: error.message });
  }
}

module.exports = {
  removeAdmin,
};
