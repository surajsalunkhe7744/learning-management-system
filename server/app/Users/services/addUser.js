const joi = require("@hapi/joi");
const bcrypt = require("bcrypt");

const { user } = require("../../../models");
const { schema } = require("../Validation/userValidation");

async function addUser(req, res) {
  try {
    let duplicateUserHandler = await user.findOne({
      where: {
        email: req.body.email,
      },
    });

    if (duplicateUserHandler === 0) {
      return res
        .status(400)
        .json({ message: "User already exist for mention email id" });
    }

    await schema.validateAsync(req.body, { abortEarly: false });

    req.body.password = bcrypt.hashSync(req.body.password, 10);
    const addedUser = await user.create(req.body);
    return res.status(200).json({
      addedUser,
    });
  } catch (error) {
    return res.status(500).json({ error: error.message });
  }
}

module.exports = {
  addUser
};
