const { Op } = require("sequelize");

const { user } = require("../../../models");

async function removeUser(req, res) {
    try {
        let removedUser = await user.destroy({
            where: {
                    email: req.userData.email,
                }
        });

        if(removedUser !== 0 && removedUser !== undefined) {
            res.status(200).json({
                Message: `The user was removed successfully.`
            })
        } else {
            res.status(404).json({
                Message: `The user was not found.`
            })
        } 
    } catch(error) {
        return res.status(500).json({
            message: error.message
        });
    }
}

module.exports = {
    removeUser
}