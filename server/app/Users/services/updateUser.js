const bcrypt = require("bcrypt");

const { user } = require("../../../models");
const { schema } = require("../Validation/userValidation");

async function updateUserData(req, res) {
  try {
    // console.log(req.body); 
    await schema.validateAsync(req.body);

    if (req.body.password !== undefined && req.body.password !== null && req.body.hasOwnProperty("password")) {
      req.body.password = bcrypt.hashSync(req.body.password, 10);
    }

    let updatedUser = await user.update(req.body, {
      where: {
        id: req.userData.id,
      },
    });

    // console.log(updatedUser);

    if(updatedUser !== undefined && updatedUser[0] === 1) {
      res.status(200).json({ message: `User data is updated now.` });
    } else {
      res.status(404).json({ message: `User was not found.` });
    }
  } catch (error) {
    console.log(error);
    return res.status(500).json({ error: error.message });
  }
}

module.exports = {
  updateUserData,
};
