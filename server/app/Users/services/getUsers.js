const { user } = require('../../../models');

async function getUsers(req,res) {
    try {
        const usersData = await user.findAll();
        return res.status(200).json(usersData);
    } catch (error) {
        return res.status(500).send(error.message);
    }
}

module.exports = {
    getUsers
}