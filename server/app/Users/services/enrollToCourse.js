const { Op } = require("sequelize");

const { user_courses } = require("../../../models");

async function userCourseEnrollment(req, res) {
    console.log(req.body);
    try {
        let enrolledCoursesByUser = await user_courses.findOne({
            where: {
                    courseId: req.body.courseId,
                    userId: req.body.userId
                }
        });

        console.log(enrolledCoursesByUser);

        if(enrolledCoursesByUser !== undefined && enrolledCoursesByUser !== null) {
            res.status(400).json({
                Message: `Course was already enrolled.`
            })
        } else {
            const courseEnrolled = await user_courses.create(req.body);
            return res.status(200).json({
                courseEnrolled,
            });
        } 
    } catch(error) {
        return res.status(500).json({
            message: error.message
        });
    }
}

module.exports = {
    userCourseEnrollment
}