const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
require("dotenv").config();

const { user } = require("../../../models");
const { schema } = require("../Validation/userValidation");

const SECRET_KEY = process.env.SECRET;

async function userLoginHandler(req, res) {
  try {
    let { email, password } = req.body;

    await schema.validateAsync(req.body);
    let isAvailable = await user.findOne({
      where: {
        email,
      },
    });

    if (!isAvailable) {
      return res.status(400).json({ message: "User not exist" });
    }

    let passMatch = await bcrypt.compare(password, isAvailable.password);

    if (!passMatch) {
      return res.status(400).json({ message: "Password is incorrect" });
    }

    // console.log(isAvailable);

    const token = jwt.sign(
      {
        id: isAvailable.id,
        email: email,
        isAdmin: isAvailable.isAdmin,
      },
      SECRET_KEY,
      (err, token) => {
        !err
          ? res.status(200).json({
              message: "Authenticate Successful",
              token: token,
              user: isAvailable
            })
          : res.status(500).json({
              message: "Something went wrong ... ",
            });
      }
    );
  } catch (error) {
    return res.status(500).json({
      message: error.message,
    });
  }
}

module.exports = {
  userLoginHandler,
};
