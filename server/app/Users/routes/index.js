const { Router } = require('express');

const userControllers = require('../controllers/user');
const checkAuthMiddleware = require("../middleware/check-auth");

const router = Router();

router.get("/all/users", userControllers.getAllUser);
router.get("/user/profile", checkAuthMiddleware.checkAuth,userControllers.getSingleUser);
router.post("/user/create", userControllers.createUser);
router.post("/user/course/enrolled", checkAuthMiddleware.checkAuth,userControllers.enrollToCourse);
router.patch("/user/update", checkAuthMiddleware.checkAuth,userControllers.updateUser);
router.post("/user/login", userControllers.userLogin);
router.delete("/user/remove", checkAuthMiddleware.checkAuth,userControllers.deleteUser);
router.patch("/user/admin/:id", checkAuthMiddleware.checkAuth,userControllers.studentToAdmin);
router.patch("/user/student/:id", checkAuthMiddleware.checkAuth,userControllers.adminToStudent);

module.exports = router;