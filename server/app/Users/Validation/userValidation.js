const joi = require("@hapi/joi");

const schema = joi.object({
    firstName: joi.string().pattern(new RegExp("^[a-zA-Z]{1,40}$")),
    lastName: joi.string().pattern(new RegExp("^[a-zA-Z]{1,40}$")),
    email: joi.string().email({ tlds: { allow: false } }),
    password: joi.string().pattern(new RegExp("^[a-zA-Z0-9]{3,16}$")),
    repassword: joi.ref("password"),
  });

module.exports = {
    schema
}