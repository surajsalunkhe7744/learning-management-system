const { Router } = require('express');
const coursesControllers = require('../controllers/course');
const checkAuthMiddleware = require('../../Users/middleware/check-auth');
const router = Router();

// console.log(checkAuthMiddleware.checkAuth());
router.post('/course/create', checkAuthMiddleware.checkAuth, coursesControllers.createCourse);
router.delete('/course/remove/:id', checkAuthMiddleware.checkAuth,coursesControllers.deleteCourse);
router.get('/courses', coursesControllers.getAllCourses);
router.patch('/course/update/:id', checkAuthMiddleware.checkAuth,coursesControllers.updateOneCourse);

module.exports = router;