const { course } = require("../../../models");

async function removeCourse(req, res) {
    // console.log(req.params.id);
    try {
        let removedCourse = await course.destroy({
            where: {
                id: req.params.id
            }
        });

        if(!req.userData.isAdmin) {
            return res.status(400).json({
                Message: `The admin user should only allow to remove the course.`
            })
        }

        removedCourse
        ?  res.status(200).json({
            Message: `The course of id ${req.params.id} was removed successfully.`
        })
        : res.status(404).json({
            Message: `The course of id ${req.params.id} was not found.`
        })
    } catch(error) {
        return res.status(500).json({
            message: error.message
        });
    }
}

module.exports = {
    removeCourse
}