const joi = require("@hapi/joi");

const { course } = require("../../../models");

async function updateCourse(req, res) {
  try {
    let courseData = {
      image: req.body.image,
      title: req.body.title,
      content: req.body.content,
    };

    const schema = joi.object({
        image: joi.string().min(3).max(100),
        title: joi.string().pattern(new RegExp("^[a-zA-Z ]{3,30}$")),
        content: joi.string(),
    });

    await schema.validateAsync(req.body);

    let updatedCourse = await course.update(courseData, {
      where: {
        id: req.params.id,
      },
    });

    updatedCourse[0] === 1
      ? res
          .status(200)
          .json({ message: `course of id ${req.params.id} is updated now.` })
      : res
          .status(404)
          .json({ message: `course of id ${req.params.id} was not found.` });

  } catch (error) {
    return res.status(500).json({
      message: error.message,
    });
  }
}

module.exports = {
  updateCourse,
};
