const joi = require("@hapi/joi");

const { course } = require('../../../models');

async function addCourse(req,res) {
    try {

        const schema = joi.object({
            image: joi.string().min(3).max(100),
            title: joi.string().pattern(new RegExp("^[a-zA-Z ]{3,30}$")),
            content: joi.string(),
        });
    
        await schema.validateAsync(req.body);

        const courseData = await course.create(req.body);
        return res.status(201).json({
            courseData,
        });
    } catch (error) {
        return res.status(500).json({ message: `Please provide valid course data ${error.message}` });
    }
}

module.exports = {
    addCourse
}