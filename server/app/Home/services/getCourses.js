const { course } = require('../../../models');

async function getCourses(req,res) {
    try {
        const courses = await course.findAll();
        return res.status(200).json(courses);
    } catch (error) {
        return res.status(500).send(error.message);
    }
}

module.exports = {
    getCourses
}