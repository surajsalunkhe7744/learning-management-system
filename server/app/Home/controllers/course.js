const { addCourse } = require("../services/addCourse");
const { getCourses } = require("../services/getCourses");
const { removeCourse } = require("../services/removeCourse");
const { updateCourse } = require("../services/updateCourse");

const createCourse = (req, res) => {
    addCourse(req,res);
}

const getAllCourses =  (req, res) => {
    getCourses(req,res);
}

const deleteCourse =  (req, res) => {
    removeCourse(req,res);
}

const updateOneCourse =  (req, res) => {
    updateCourse(req,res);
}

module.exports = {
    createCourse,
    getAllCourses,
    deleteCourse,
    updateOneCourse
}