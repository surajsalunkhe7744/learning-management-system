const express = require("express");
const bodyParser = require('body-parser');
const cors = require("cors");
const app = express();
app.use(cors());
const homeRoutes = require("./app/Home/routes/index");
const userRoutes = require("./app/Users/routes/index");


const PORT = process.env.PORT || 8083;
// sudo kill -9 $(sudo lsof -t -i:9001)

app.use(bodyParser.json());

app.use('/api', homeRoutes);
app.use('/api', userRoutes);


app.listen(PORT, () => console.log(`Listening on port: ${PORT}`))